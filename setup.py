"""
revit-py setup
"""

from distutils.core import setup

setup(
      name='revit',
      packages=['revit'],
      version='0.2',
      description='Cli interface for rev, online code review tool',
      author='brwnrclse (Barry Harris)',
      author_email='dev@vaemoi.co',
      url='https://rev.vaemoi.co',
      download_url='https://bitbucket.org/vaemoi/revit-py/get/master.tar.gz',
      keywords=['code review', 'rev', 'oss'],
      classifiers=[],
)
